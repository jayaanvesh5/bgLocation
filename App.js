import React from 'react';
import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  Button,
  View,
  PermissionsAndroid,
  Platform,
  AppState,
} from 'react-native';
import ReactNativeForegroundService from '@supersami/rn-foreground-service';
import Geolocation from '@react-native-community/geolocation';

class App extends React.Component {
  constructor() {
    super();
    this.state = {
      appState: AppState.currentState,
      permisMsg: 'waiting',
      samNO: 0,
    };
  }

  async componentDidMount() {
    // listener to check the App state
    PermissionsAndroid.requestMultiple([
      'android.permission.ACCESS_COARSE_LOCATION',
      'android.permission.ACCESS_FINE_LOCATION',
    ]).then((res) => {
      console.log(res);
    });
    AppState.addEventListener('change', this._handleAppStateChange);
    Geolocation.getCurrentPosition((info) => console.log('called'));
  }

  // to remove all listeners
  componentWillUnmount() {
    AppState.removeEventListener('change', this._handleAppStateChange);
  }

  // App state handler to know the state of App (inactive / background / foreground)
  _handleAppStateChange = (nextAppState) => {
    if (this.state.appState.match(/inactive|background/) && nextAppState === 'active') {
      console.log('App Came to foreground');
    }
    this.setState({ appState: nextAppState });
  };

  startTask = () => {
    if (Platform.OS == 'android') {
      // this.permissionsHandler();
      console.log('called 2');
      ReactNativeForegroundService.add_task(() => this.sampleApiCall(), {
        delay: 1000,
        onLoop: true,
        taskId: 'bgLocTask',
        onError: (e) => console.log(`Error logging:`, e),
        onSuccess: (e) =>
          console.log(
            'App is in',
            this.state.appState == 'active' ? 'fore ground' : this.state.appState
          ),
      });
      console.log('called 4');
      ReactNativeForegroundService.start({
        id: 'bgLocTask',
        title: 'Foreground Service',
        message: 'you are online!',
      });
      // let abc = ReactNativeForegroundService.get_all_tasks();
      let def = ReactNativeForegroundService.get_task('bgLocTask');
      let ghi = ReactNativeForegroundService.is_task_running('bgLocTask');
      // console.log(abc, 'get_all_tasks');
      console.log(def, 'get_task');
      console.log(ghi, 'is_task_running');
    }
  };

  stopTask = () => {
    if (Platform.OS == 'android') {
      ReactNativeForegroundService.stop({
        id: 'bgLocTask',
        title: 'Foreground Service',
        message: 'you are online!',
      });
    }
  };

  sampleApiCall = async () => {
    let samNO = this.state.samNO;
    samNO = samNO + 1;
    this.setState({ samNO });
    Geolocation.getCurrentPosition(
      async (info) => {
        console.log(info, this.state.samNO);
        if (info) {
          await fetch(`https://tracker-qa.shahen-sa.com/api/v1/location/driverCurrentLocation`, {
            method: 'POST',
            headers: {
              Accept: 'application/json',
              'Content-Type': 'application/json',
            },
            body: JSON.stringify({
              driverID: 'DRX10007',
              lat: info.coords.latitude,
              long: info.coords.longitude,
              mobileNumber: '919542081736',
            }),
          })
            .then((response) => response.json())
            .then((data) => {
              console.warn('XXXXXXX', JSON.stringify(data));
            })
            .catch((error) => {
              console.log('Current Location Error: ' + error.message);
            });
        }
      },
      (error) => {
        console.log(error);
      }
      // {
      //   accuracy: {
      //     android: 'high',
      //     ios: 'best',
      //   },
      //   enableHighAccuracy: true,
      //   timeout: 5000,
      //   maximumAge: 3000,
      //   distanceFilter: 0,
      //   forceRequestLocation: true,
      //   showLocationDialog: true,
      // }
    );
    // fetch('https://jsonplaceholder.typicode.com/todos/1')
    //   .then((response) => response.json())
    //   .then((json) => console.log(json, 'sample call', this.state.samNO));
  };

  render() {
    const { permisMsg } = this.state;
    return (
      <SafeAreaView>
        <StatusBar />
        <ScrollView contentInsetAdjustmentBehavior='automatic'>
          <Text>Hiiii</Text>
          <View style={{ alignSelf: 'center' }}>
            <View style={{ marginTop: 50 }}>
              <Button title='startTask' onPress={() => this.startTask()}></Button>
            </View>
            <View style={{ marginTop: 50 }}>
              <Button title='startTask' onPress={() => this.stopTask()}></Button>
            </View>
          </View>
          <Text>{permisMsg}</Text>
        </ScrollView>
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  sectionContainer: {
    marginTop: 32,
    paddingHorizontal: 24,
  },
  sectionTitle: {
    fontSize: 24,
    fontWeight: '600',
  },
  sectionDescription: {
    marginTop: 8,
    fontSize: 18,
    fontWeight: '400',
  },
  highlight: {
    fontWeight: '700',
  },
});

export default App;
